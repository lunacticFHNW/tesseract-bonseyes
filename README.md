# Tesseract 4 OCR Runtime Environment - Docker Container for Bonseyes

Based on [Alexander Pozdnyakov's](https://launchpad.net/~alex-p) [Personal Package Archive](https://en.wikipedia.org/wiki/Personal_Package_Archive) [`tesseract-ocr`](https://launchpad.net/~alex-p/+archive/ubuntu/tesseract-ocr/+packages) - Tesseract command line OCR tool. If you want to compile Tesseract 4 yourself please take look at [tesseractshadow/tesseract4cmp](https://hub.docker.com/r/tesseractshadow/tesseract4cmp/).

If you are not familiar with Docker please read [Docker - Get Started](https://docs.docker.com/get-started/).

## Goal
The goal of this repository is to provide an overview of how Docker Containers could be built that can be shared within the Bonseyes Ecosystem.

The goal is to have a Docker Image that contains the following parts:
- A descriptor (in this case `ai-app.yml`)
- A license (in this case `license.json`)
- The payload (in this case the Tesseract application)

In order to build the image a `Dockerfile` is provided that contains all instructions to build the image that could be distributed.


## Quick start
Prerequisites:
* [Install Docker](https://docs.docker.com/engine/installation/)
* Build Docker Image with:
  
  ```docker build -t bonseyes/tesseract4cmp .```


Scripted steps (`cd ./scripts/`):
1. Run the image as the **t4re** `./run.sh` or just start it if is stopped `./start.sh`.
2. Do some OCR test: `./test.sh`.
3. Stop **t4re** container instance: `./stop.sh`.
